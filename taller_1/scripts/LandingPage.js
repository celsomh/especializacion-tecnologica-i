var comentario;
var comentarios = [];
var vistaComentarios = document.getElementById("vista-comentarios");

var btnEnviarComentario = document.getElementById("btn-enviar-comentario");
btnEnviarComentario.addEventListener("click", function() {
    comentario = document.getElementById("comentario");
    if (comentario.value == "") {
        alert("Por favor, ingrese un comentario");
    } else {
        comentarios.push(comentario.value);
        vistaComentarios.innerHTML = "";
        var inicio;

        if (comentarios.length <= 4) {
            inicio = 0;
        } else {
            inicio = comentarios.length - 4;
        }
        for (var i = inicio; i < comentarios.length; i++) {
            vistaComentarios.innerHTML += "<p>" + comentarios[i] + "</p>";
        }
        comentario.value = "";
    }


});