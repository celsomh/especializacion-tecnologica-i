//Ejemplo sencillo de componente
Vue.component("item-vue", {
    props: ["titulo", "subtitulo"],
    //Usar acento invertido
    template: `
    <h1>
        <div>
            <h1>
                {{ titulo }}
            </h1>
            <h2>
                {{ subtitulo }}
            </h2>
        </div>
    </h1>
    `
})

var vm = new Vue({
    el: '#app', //Este siempre va primero
    data: {
        //Lo que se almacena en datos
        message: "Hello World!",
        numb: 1,
        fNumb: 1.0,
        array: [{ nombre: "nombre1" }, { nombre: "nombre2" }, { nombre: "nombre3" }],
        array2: ["123", "456", "789"]
    },
    methods: {
        showAlert: function() {
            alert("Hola Mundo!")
        },
        //Agregar métodos aqui, asi como el anterior
        addNombre: function(name) {
            this.array.push({ nombre: name });
        }
    }
});