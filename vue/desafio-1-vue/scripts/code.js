Vue.component("fila-producto", {
    props: ["producto"],
    //De momento no se incluye la opción editar y eliminar
    template: `
    <tr>
        <td>
        <input type="text" v-model="producto.nombre" disabled="disabled" />
            
        </td>
        <td>
            <input type="text" v-model="producto.descripcion" disabled="disabled" />
        </td>
        <td>
            <input type="number" v-model="producto.precio" disabled="disabled" />
        </td>
        <td>
            <button onclick="habilitarEdicion(this)" >Editar</button>
        </td>
        <td>
            <button onclick="removerProducto(this)" >Eliminar</button>
        </td>
    </tr>
    `
});

var vm = new Vue({
    el: '#app',
    data: {
        id: "",
        nombre: "",
        descripcion: "",
        precio: "",
        listaProductos: []
    },
    methods: {
        anadirProducto: function(nom, des, pre) {
            pre = parseInt(pre);
            if (pre > 0) {
                if (!(nom.localeCompare("") == 0)) {
                    var i = this.listaProductos.length + 1;
                    this.listaProductos.push({ id: i, nombre: nom, descripcion: des, precio: pre });
                } else {
                    alert("Debe ingresar un nombre");
                }
            } else {
                alert("No se permiten precios negativos");
            }

        }
    }
});

//Remover producto de la tabla, mas no de la listaProductos
function removerProducto(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

function habilitarEdicion(btn) {
    var row = btn.parentNode.parentNode;
    var elemNombre;
    var elemDescripcion;
    var elemPrecio;

    if (btn.textContent.localeCompare("Editar") == 0) {
        elemNombre = row.cells[0].childNodes[0];
        elemNombre.disabled = false;
        elemDescripcion = row.cells[1].childNodes[0];
        elemDescripcion.disabled = false;
        elemPrecio = row.cells[2].childNodes[0];
        elemPrecio.disabled = false;
        btn.innerHTML = "Aceptar";
    } else if (btn.textContent.localeCompare("Aceptar") == 0) {
        elemNombre = row.cells[0].childNodes[0];
        elemNombre.disabled = true;
        elemDescripcion = row.cells[1].childNodes[0];
        elemDescripcion.disabled = true;
        elemPrecio = row.cells[2].childNodes[0];
        elemPrecio.disabled = true;
        btn.innerHTML = "Editar";
    }
}