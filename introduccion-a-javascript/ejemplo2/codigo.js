var btnSumar = document.getElementById("btn-sumar");
var btnRestar = document.getElementById("btn-restar");
var btnMultiplicar = document.getElementById("btn-multiplicar");
var btnDividir = document.getElementById("btn-dividir");

var resultado = document.getElementById("resultado");
var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");

btnSumar.addEventListener("click", function() {
    hacerOperacion(inputUno, inputDos, 1);
});
btnRestar.addEventListener("click", function() {
    hacerOperacion(inputUno, inputDos, 2);
});
btnMultiplicar.addEventListener("click", function() {
    hacerOperacion(inputUno, inputDos, 3);
});
btnDividir.addEventListener("click", function() {
    hacerOperacion(inputUno, inputDos, 4)
});

function hacerOperacion(n1, n2, type) {

    if (n1.value != "" && n2.value != "") {
        resultado.style.color = "black"
        n1 = parseFloat(n1.value);
        n2 = parseFloat(n2.value);
        if (Number.isInteger(n1) && Number.isInteger(n2)) {
            n1 = parseInt(n1);
            n2 = parseInt(n2);
            switch (type) {
                case 1:
                    resultado.innerHTML = n1 + n2;
                    break;

                case 2:
                    resultado.innerHTML = n1 - n2;
                    break;

                case 3:
                    resultado.innerHTML = n1 * n2;
                    break;

                case 4:
                    if (n2 == 0) {
                        resultado.style.color = "red";
                        resultado.innerHTML = "División por 0";

                        break;
                    }
                    resultado.innerHTML = n1 / n2;
                    break;
            }
        } else {
            resultado.style.color = "red";
            resultado.innerHTML = "Solo se permiten números enteros";
        }
    } else {
        resultado.style.color = "red";
        resultado.innerHTML = "Se deben ingresar ambos números";
    }
}