//Comentario de una linea
/* 
Comentario 
de 
varias
lineas
*/

var edad = 21;
var precio = 2.99;
var pagado = true;
var nombre = "Atila";

//Mosrar por consola
console.log(nombre);

//Concatenación
var concat = "Hola " + "mi nombre es " + nombre + " y tengo " + edad + " años";

//Operadores artiméticos
var dobleEdad = edad * 2;
console.log(dobleEdad);

var mitadEdad = edad / 2;

//Incremento, decremento

var numero = 5;
++numero;
console.log(numero);

--numero;
console.log(numero);

//Operadores lógicos

var valor1 = true;
var valor2 = false;
var resultado = valor1 && valor2;

valor1 = true;
valor2 = true;
var valor3 = true;
resultado = valor1 && valor2 && valor3;

valor1 = true;
valor2 = false;
resultado = valor1 || valor2;

valor1 = false;
valor2 = false;
resultado = valor1 || valor2;

//Operadores de relación

valor1 = true;
valor2 = true;
resultado = valor1 == valor2;

valor1 = false;
valor2 = false;
resultado = valor1 != valor2;

var n1 = 6
var n2 = 12;
resultado = n1 < n2;

n1 = 3;
n2 = 9;
resultado = n1 > n2;

var visible = true;
alert(!visible);

//Condiciones
var edad = 18;
if (edad >= 18) {
    alert("Eres mayor de edad");
} else {
    alert("Eres menor de edad");
}
edad = 5;
if (edad >= 0 && edad < 12) {
    alert("Eres un niño");
} else if (edad >= 12 && edad < 18) {
    alert("Eres un adolescente");
} else {
    alert("Eres mayor de edad");
}

var mes = 2;
switch (mes) {
    case 1:
        alert("El mes es enero");
        break;
    case 2:
        alert("El mes es febrero");
        break;
    default:
        alert("El mes no es enero ni febrero");
        break;

}

//Arreglos y sus métodos básicos
var cantElementos = 4;
var arreglo = new Array(cantElementos);
arreglo[0] = "dato1";
arreglo[1] = "dato2";
arreglo[2] = "dato3";
arreglo[3] = "dato4";

arreglo = [1, 2, 3];
arreglo = [1, "hola", 3.3];

console.log(arreglo[0]);
console.log(arreglo[1]);
console.log(arreglo[2]);

arreglo.length;
arreglo.push("último");
arreglo.pop();
arreglo.unshift("primero");
arreglo.shift();

//Ciclos
for (var i = 10; i > 0; i--) {
    console.log("El número es: " + i);
};

var n = 0;
while (n < 3) {
    console.log("El número es: " + n++);
}

n = 2;
do {
    console.log("El número es: " + n++);
} while (n < 5);