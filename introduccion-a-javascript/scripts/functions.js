//Funciones
function nombreFuncion() {

}

nombreFuncion();

function sumaMuestra(primerNumero, segundoNumero) {
    var resultado = primerNumero + segundoNumero;
    console.log("El resultado es: " + resultado);
}
sumaMuestra(2, 4);

function restaMuestra(primerNumero, segundoNumero) {
    var resultado = primerNumero - segundoNumero;
    return resultado;
}

console.log(restaMuestra(6, 3));

function Persona(nombre, edad) {
    this.nombre = nombre;
    this.edad = edad;
    this.saludar = function() {
        console.log("Hola, soy " + this.nombre);
    }
}

var pedro = new Persona("Pedro", 19);
pedro.saludar();

var juan = new Persona("Juan", 24);
juan.saludar();

//JSON
var persona = {
    nombre: "Pedro",
    edad: 19,
    sexo: "M"
}

console.log(persona["nombre"]);
console.log(persona["edad"]);

console.log(persona.nombre);
console.log(persona.edad);

persona = {
    nombre: "Pedro",
    edad: 19,
    sexo: "M",
    saludar: function() {
        console.log("Hola soy " + this.nombre);
    }
}

persona.saludar();

//DOM
//Implementaciones en index.html

//Eventos