var operacion = document.getElementById("operacion");
var resultado = document.getElementById("resultado");

var numCero = document.getElementById("num-cero");
numCero.addEventListener("click", function() {
    operacion.innerHTML += 0;
});
var numUno = document.getElementById("num-uno");
numUno.addEventListener("click", function() {
    operacion.innerHTML += 1;
});
var numDos = document.getElementById("num-dos");
numDos.addEventListener("click", function() {
    operacion.innerHTML += 2;
});
var numTres = document.getElementById("num-tres");
numTres.addEventListener("click", function() {
    operacion.innerHTML += 3;
});
var numCuatro = document.getElementById("num-cuatro");
numCuatro.addEventListener("click", function() {
    operacion.innerHTML += 4;
});
var numCinco = document.getElementById("num-cinco");
numCinco.addEventListener("click", function() {
    operacion.innerHTML += 5;
});
var numSeis = document.getElementById("num-seis");
numSeis.addEventListener("click", function() {
    operacion.innerHTML += 6;
});
var numSiete = document.getElementById("num-siete");
numSiete.addEventListener("click", function() {
    operacion.innerHTML += 7;
});
var numOcho = document.getElementById("num-ocho");
numOcho.addEventListener("click", function() {
    operacion.innerHTML += 8;
});
var numNueve = document.getElementById("num-nueve");
numNueve.addEventListener("click", function() {
    operacion.innerHTML += 9;
});

var signoSuma = document.getElementById("signo-suma");
signoSuma.addEventListener("click", function() {
    operacion.innerHTML += "+";
});

var signoResta = document.getElementById("signo-resta");
signoResta.addEventListener("click", function() {
    operacion.innerHTML += "-";
});

var signoMultiplicacion = document.getElementById("signo-multiplicacion");
signoMultiplicacion.addEventListener("click", function() {
    operacion.innerHTML += "*";
});

var signoDivision = document.getElementById("signo-division");
signoDivision.addEventListener("click", function() {
    operacion.innerHTML += "/";
});

var signoClear = document.getElementById("signo-clear");
signoClear.addEventListener("click", function() {
    limpiarCalculadora();
});

var signoClearError = document.getElementById("signo-clear-error");
signoClearError.addEventListener("click", function() {
    operacion.innerHTML = "";
});

var signoIgual = document.getElementById("signo-igual");
signoIgual.addEventListener("click", function() {
    resultado.innerHTML = calcularResultado();
});

var signoPunto = document.getElementById("signo-punto");
signoPunto.addEventListener("click", function() {
    operacion.innerHTML += ".";
});

var cambioSigno = document.getElementById("cambio-signo");
cambioSigno.addEventListener("click", function() {
    operacion.innerHTML = (parseFloat(operacion.textContent)) * (-1);
});

document.addEventListener("keydown", function(event) {
    if (event.keyCode == 49) {
        operacion.innerHTML += 1;
    } else if (event.keyCode == 50) {
        operacion.innerHTML += 2;
    } else if (event.keyCode == 51) {
        operacion.innerHTML += 3;
    } else if (event.keyCode == 52) {
        operacion.innerHTML += 4;
    } else if (event.keyCode == 53) {
        operacion.innerHTML += 5;
    } else if (event.keyCode == 54) {
        operacion.innerHTML += 6;
    } else if (event.keyCode == 55) {
        operacion.innerHTML += 7;
    } else if (event.keyCode == 56) {
        operacion.innerHTML += 8;
    } else if (event.keyCode == 57) {
        operacion.innerHTML += 9;
    } else if (event.keyCode == 48) {
        operacion.innerHTML += 0;
    } else if (event.keyCode == 190) {
        operacion.innerHTML += ".";
    } else if (event.keyCode == 187) {
        operacion.innerHTML += "+";
    } else if (event.keyCode == 189) {
        operacion.innerHTML += "-";
    } else if (event.keyCode == 106) {
        operacion.innerHTML += "*";
    } else if (event.keyCode == 111) {
        operacion.innerHTML += "/";
    } else if (event.keyCode == 8) {
        var strTemp = operacion.innerHTML;
        if (strTemp.length > 0) {
            operacion.innerHTML = strTemp.substring(0, strTemp.length - 1);
        }
    } else if (event.keyCode == 13) {
        resultado.innerHTML = calcularResultado();
    } else if (event.keyCode == 46) {
        limpiarCalculadora();
    }
});

function calcularResultado() {
    var numeroNegativo = false;
    if (operacion.textContent.startsWith("-")) {
        numeroNegativo = true;
    }
    var strOperacion = operacion.textContent;
    if (strOperacion.includes("+")) {
        var arrOperacion = strOperacion.split("+")
        var num1 = parseFloat(arrOperacion[0]);
        var num2 = parseFloat(arrOperacion[1]);
        return (num1 + num2);

    } else if (strOperacion.includes("*")) {
        var arrOperacion = strOperacion.split("*");
        var num1 = parseFloat(arrOperacion[0]);
        var num2 = parseFloat(arrOperacion[1]);
        return (num1 * num2);
    } else if (strOperacion.includes("/")) {
        var arrOperacion = strOperacion.split("/");
        var num1 = parseFloat(arrOperacion[0]);
        var num2 = parseFloat(arrOperacion[1]);
        return (num1 / num2);
    } else if (strOperacion.includes("-")) {
        var arrOperacion = strOperacion.split("-");
        var num1;
        var num2;
        if (arrOperacion.length == 2) {
            num1 = parseFloat(arrOperacion[0]);;
            num2 = parseFloat(arrOperacion[1]);
            return (num1 - num2);
        } else {
            if (arrOperacion[0] == "") {
                num1 = parseFloat(arrOperacion[1]) * (-1);
            } else {
                num1 = parseFloat(arrOperacion[0]);
            }
            if (arrOperacion[arrOperacion.length - 2] == "") {
                num2 = parseFloat(arrOperacion[arrOperacion.length - 1]) * (-1);

            } else {
                num2 = parseFloat(arrOperacion[arrOperacion.length - 1]);
            }

            return (num1 - num2);
        }
    }
}

function limpiarCalculadora() {
    operacion.innerHTML = "";
    resultado.innerHTML = "";
}