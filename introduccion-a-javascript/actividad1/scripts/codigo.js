var numMin = document.getElementById("num-min");
var numMax = document.getElementById("num-max");
var btnGenerar = document.getElementById("btn-generar");
var resultado = document.getElementById("resultado");
btnGenerar.addEventListener("click", function() {
    resultado.innerHTML = generarNumeroAleatorio(numMin.value, numMax.value);
});

function generarNumeroAleatorio(min, max) {
    min = parseInt(min);
    max = parseInt(max);
    if (min > max) {
        return alert("El número minimo es mayor");
    }
    return (Math.floor(Math.random() * ((max + 1) - min)) + min);
}